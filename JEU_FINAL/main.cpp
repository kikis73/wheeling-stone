#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <windows.h>
#include <mmsystem.h>
#define PAS_T 1.f
#define PAS_RAMPE 5
#define HAUTEUR_JUL 149
#define LARGEUR_JUL 155
#define POS_X_JUL 400
#define PAUSE
#define POS_Y_JUL 400
#define LARGEUR_FENETRE 1920
#define HAUTEUR_FENETRE 1080
#define EPAISSEUR_RAMPE 25
#define LONGUEUR_PALIER_1 700
#define LONGUEUR_PALIER_2 300
#define LONGUEUR_DESCENTE_1 415
#define LONGUEUR_MONTEE_1 250
#define LONGUEUR_MONTEE_2 300
#define LONGUEUR_MONTEE_3 600
#define LONGUEUR_PALIER_3 600
#define LONGUEUR_PALIER_4 400
#define LONGUEUR_MONTEE_4 600
#define LONGUEUR_PALIER_5 700
#define LONGUEUR_DESCENTE_2 910
#define LONGUEUR_PALIER_6 950
#define LONGUEUR_MONTEE_5 600
#define LONGUEUR_PALIER_7 800
#define LONGUEUR_DESCENTE_3 600




using namespace sf;

int main()
{

    //fen�tre du menu

    RenderWindow menuapp(VideoMode(1280, 720), "MENU");
    Event event;
    Texture image5;
    image5.loadFromFile("menu.png");
    Sprite menu;
    menu.setTexture(image5);

    while (menuapp.isOpen())
    {

        while (menuapp.pollEvent(event))
        {
            if (event.type == Event::KeyPressed && event.key.code == Keyboard::Numpad2)
                menuapp.close(); // fermeture des 2 fen�tres avec touche "2"



            if (event.type == Event::KeyPressed && event.key.code == Keyboard::Numpad1)
            {
                menuapp.close();  // fermeture seulement de la fen�tre menu avec la touche "1"
            }
        }

        // Clear screen
        menuapp.clear();

        // Draw the sprite
        menuapp.draw(menu);
        menuapp.display();


    }

    int choix=0; // variable pour le retry mais pas finis
    int gagne=0; // idem
    int mort=0; // compteur de mort, a finir
    int cabre = 0; // �tat du cabre
    int debut = 0;

    // initialisation des positions des sprites/shapes

    // jul
    int positionXJul=((1.0/3)*LARGEUR_FENETRE);
    int positionYJul=((2.0/3)*HAUTEUR_FENETRE);
    //rampe
    int positionXRampe = 0;
    int positionYRampe = (2.0/3)*HAUTEUR_FENETRE;
   //montee1
    int positionXmontee_1 = (((1.0/3)*LARGEUR_FENETRE)*2)+EPAISSEUR_RAMPE-30;
    int positionYmontee_1 =(2.0/3)*HAUTEUR_FENETRE+EPAISSEUR_RAMPE-EPAISSEUR_RAMPE+15;
   // palier 1
    int positionXpalier_1 = ((1.0/3)*LARGEUR_FENETRE*2)+EPAISSEUR_RAMPE-55+LONGUEUR_PALIER_1;
    int positionYpalier_1 =(2.0/3)*HAUTEUR_FENETRE+EPAISSEUR_RAMPE-EPAISSEUR_RAMPE+17 ;
   // montee2
    int positionXmontee_2 = positionXpalier_1+283;
    int positionYmontee_2 = positionYpalier_1-60;
    // palier2
    int positionXpalier_2 = positionXmontee_2-55+LONGUEUR_PALIER_2;
    int positionYpalier_2=positionYmontee_2-EPAISSEUR_RAMPE+15;
    // descente 1
    int positionXdescente_1=positionXpalier_2-50;
    int positionYdescente_1=positionYpalier_2+(LONGUEUR_DESCENTE_1/2)-EPAISSEUR_RAMPE+19;
    // palier3
    int positionXpalier_3=LARGEUR_FENETRE*2;
    int positionYpalier_3=(2.0/3)*HAUTEUR_FENETRE;
    // montee3
    int positionXmontee_3 = positionXpalier_3+LONGUEUR_PALIER_3+LONGUEUR_PALIER_3-LONGUEUR_PALIER_4;
    int positionYmontee_3 = positionYRampe+EPAISSEUR_RAMPE+EPAISSEUR_RAMPE;
    // palier4
    int positionXpalier_4=positionXmontee_3-85+LONGUEUR_PALIER_4-290;
    int positionYpalier_4=positionYRampe+EPAISSEUR_RAMPE+EPAISSEUR_RAMPE-6;
    // montee4
    int positionXmontee_4=positionXpalier_4+LONGUEUR_PALIER_4+LONGUEUR_PALIER_4+87;
    int positionYmontee_4=positionYpalier_4-333;
    // palier 5
    int positionXpalier_5=positionXmontee_4+680;
    int positionYpalier_5=positionYmontee_4+(positionYmontee_4/2)+53;
    // descente 2
    int positionXdescente_2=positionXpalier_5;
    int positionYdescente_2=positionYpalier_5-(LONGUEUR_DESCENTE_2/2)+EPAISSEUR_RAMPE;
    // palier 6
    int positionXpalier_6 = positionXdescente_2+748+LONGUEUR_PALIER_6;
    int positionYpalier_6 = positionYRampe;
    // montee5
    int positionXmontee_5=positionXpalier_6;
    int positionYmontee_5=positionYRampe;
    // paleir 7
    int positionXpalier_7=positionXmontee_5+520;
    int positionYpalier_7=418;
    //descente 3
    int positionXdescente_3=positionXpalier_7 +LONGUEUR_PALIER_7;
    int positionYdescente_3=420;
    // drapeau
    int positionXdrapeau = positionXdescente_3+1300;
    int positionYdrapeau = positionYRampe -300;






    // Create the main window
    RenderWindow app(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "JEU", Style::Fullscreen);
    float rotation = 360;
    Texture image1; // background
    Texture image2; // jul
    Texture image3; // game over
    Texture image4; // bavette, pas finis
    Texture image6; // drapeau
    Texture image7; // victory

    // image
    image1.loadFromFile("background.jpg");
    image2.loadFromFile("jul.png");
    image3.loadFromFile("perdu.jpg");
    image4.loadFromFile("bavette.png");
    image6.loadFromFile("drapeau.png");
    image7.loadFromFile("logo.png");

    // initialisation du son
    SoundBuffer buffeurJul;
    buffeurJul.loadFromFile("jul.wav");
    Sound sondJul;
    sondJul.setBuffer(buffeurJul);
    SoundBuffer buffeurBep;
    buffeurBep.loadFromFile("bep.wav");
    Sound sondBep;
    sondBep.setBuffer(buffeurBep);

    Sprite background;
    Sprite jul;
    Sprite perdu;
    Sprite bavette;
    Sprite drapeau;
    Sprite victory;

    Vector2u size = image1.getSize();
    background.setTexture(image1);
    jul.setTexture(image2);
    perdu.setTexture(image3);
    bavette.setTexture(image4);
    menu.setTexture(image5);
    drapeau.setTexture(image6);
    victory.setTexture(image7);


    background.setScale(2.0f, 3.0f);
    perdu.setScale(2.5f, 2.5f);
    background.setOrigin(size.x / 2, size.y / 2);
    jul.setOrigin(10, HAUTEUR_JUL-20);
    jul.setRotation(0);


    RectangleShape rampe(Vector2f(LARGEUR_FENETRE,EPAISSEUR_RAMPE));
    rampe.setFillColor(Color::Red);
    rampe.setPosition(positionXRampe,positionYRampe);

    // position des formes/shapes

    jul.setPosition(positionXJul,positionYJul);

    RectangleShape montee_1(Vector2f(LONGUEUR_MONTEE_1,EPAISSEUR_RAMPE));
    montee_1.rotate(-25);
    montee_1.setPosition(positionXmontee_1,positionYmontee_1);
    montee_1.setFillColor(Color::Red);

    RectangleShape palier_1(Vector2f(LONGUEUR_PALIER_1,EPAISSEUR_RAMPE));
    palier_1.setPosition(positionXpalier_1,positionYpalier_1);
    palier_1.setFillColor(Color::Red);

    RectangleShape montee_2(Vector2f(LONGUEUR_MONTEE_2,EPAISSEUR_RAMPE));
    montee_2.rotate(-35);
    montee_2.setPosition(positionXmontee_2,positionYmontee_2);
    montee_2.setFillColor(Color::Red);

    //nouveau palier 2
    RectangleShape palier_2(Vector2f(LONGUEUR_PALIER_2,EPAISSEUR_RAMPE));
    palier_2.setPosition(positionXpalier_2,positionYpalier_2);
    palier_2.setFillColor(Color::Red);
    //nouveau descente 1
    RectangleShape descente_1(Vector2f(LONGUEUR_DESCENTE_1,EPAISSEUR_RAMPE));
    descente_1.rotate(40);
    descente_1.setPosition(positionXdescente_1,positionYdescente_1);
    descente_1.setFillColor(Color::Red);
    //nouveau montee 3
    RectangleShape palier_3(Vector2f(LONGUEUR_PALIER_3,EPAISSEUR_RAMPE));
    palier_3.setPosition(positionXpalier_3,positionYpalier_3);
    palier_3.setFillColor(Color::Red);
    //nouveau montee3
    RectangleShape montee_3(Vector2f(LONGUEUR_MONTEE_3,EPAISSEUR_RAMPE));
    montee_3.rotate(-30);
    montee_3.setPosition(positionXmontee_3,positionYpalier_3);
    montee_3.setFillColor(Color::Red);
    //nouveau palier 4
    RectangleShape palier_4(Vector2f(LONGUEUR_PALIER_4,EPAISSEUR_RAMPE));
    palier_4.setPosition(positionXpalier_4,positionYpalier_4);
    palier_4.setFillColor(Color::Red);
    //nouveau montee 4
    RectangleShape montee_4(Vector2f(LONGUEUR_MONTEE_4,EPAISSEUR_RAMPE));
    montee_4.rotate(-15);
    montee_4.setPosition(positionXmontee_4,positionYmontee_4);
    montee_4.setFillColor(Color::Red);
    //nouveau palier5
    RectangleShape palier_5(Vector2f(LONGUEUR_PALIER_5,EPAISSEUR_RAMPE));
    palier_5.setPosition(positionXpalier_5,positionYpalier_5);
    palier_5.setFillColor(Color::Red);
    //nouveau descente 2
    RectangleShape descente_2(Vector2f(LONGUEUR_DESCENTE_2,EPAISSEUR_RAMPE));
    descente_2.rotate(30);
    descente_2.setPosition(positionXdescente_2,positionYdescente_2);
    descente_2.setFillColor(Color::Red);
    //nouveau paleir 6
    RectangleShape palier_6(Vector2f(LONGUEUR_PALIER_6,EPAISSEUR_RAMPE));
    palier_6.setPosition(positionXpalier_6,positionYpalier_6);
    palier_6.setFillColor(Color::Red);
    //nouveau montee 5
    RectangleShape montee_5(Vector2f(LONGUEUR_MONTEE_5,EPAISSEUR_RAMPE));
    montee_5.rotate(-30);
    montee_5.setPosition(positionXmontee_5,positionYmontee_5);
    montee_5.setFillColor(Color::Red);
    //nouveau palier 7
    RectangleShape palier_7(Vector2f(LONGUEUR_PALIER_7,EPAISSEUR_RAMPE));
    palier_7.setPosition(positionXpalier_7,positionYpalier_7);
    palier_7.setFillColor(Color::Red);
    //nouveau descente 3
    RectangleShape descente_3(Vector2f(LONGUEUR_DESCENTE_3,EPAISSEUR_RAMPE));
    descente_3.setPosition(positionXdescente_3,positionYdescente_3);
    descente_3.setFillColor(Color::Red);
    descente_3.rotate(30);

    drapeau.setPosition(positionXdrapeau,positionYdrapeau);

    // lancement du son

    sondJul.play();



    while (app.isOpen())
    {
        // Process events
        Event event;

        while (app.pollEvent(event))
        {
            // Close window : exit
            if (event.key.code == Keyboard::Numpad2)
                app.close();
        }


        if (event.key.code == Keyboard::Numpad1)
        {
            choix=1;
        }

        // pour que le jeu ne se lance que quand l'utilisateur appuie sur le clique gauche
        if (rotation > 200)
        {
            if(event.type == Event::MouseButtonPressed)
                debut = 1;
            if(debut==1)
            {
            if(event.type == Event::MouseButtonPressed || event.type == Event::MouseButtonReleased || event.type == Event::MouseMoved)
            {
                positionXmontee_1 += -PAS_RAMPE;
                montee_1.setPosition(positionXmontee_1,positionYmontee_1);

                positionXpalier_1 += -PAS_RAMPE;
                palier_1.setPosition(positionXpalier_1,positionYpalier_1);

                positionXmontee_2 +=-PAS_RAMPE;
                montee_2.setPosition(positionXmontee_2,positionYmontee_2);
                //nouveau palier 2
                positionXpalier_2+=-PAS_RAMPE;
                palier_2.setPosition(positionXpalier_2,positionYpalier_2);
                //nouveau descente 1
                positionXdescente_1+=-PAS_RAMPE;
                descente_1.setPosition(positionXdescente_1,positionYdescente_1);
                //nouveau palier3
                positionXpalier_3+= -PAS_RAMPE;
                palier_3.setPosition(positionXpalier_3,positionYpalier_3);
                //nouveau montee3
                positionXmontee_3+= -PAS_RAMPE;
                montee_3.setPosition(positionXmontee_3,positionYmontee_3);
                //nouveau palier 4
                positionXpalier_4+= -PAS_RAMPE;
                palier_4.setPosition(positionXpalier_4,positionYpalier_4);
                //nouveau montee4
                positionXmontee_4+= -PAS_RAMPE;
                montee_4.setPosition(positionXmontee_4,positionYmontee_4);
                //nouveau palier5
                positionXpalier_5+= -PAS_RAMPE;
                palier_5.setPosition(positionXpalier_5,positionYpalier_5);
                //nouveau descente2
                positionXdescente_2 += -PAS_RAMPE;
                descente_2.setPosition(positionXdescente_2,positionYdescente_2);
                //nouveau palier6
                positionXpalier_6 += -PAS_RAMPE;
                palier_6.setPosition(positionXpalier_6,positionYpalier_6);
                //nouveau montee 5
                positionXmontee_5+= -PAS_RAMPE;
                montee_5.setPosition(positionXmontee_5,positionYmontee_5);
                //nouveau palier7
                positionXpalier_7 +=-PAS_RAMPE;
                palier_7.setPosition(positionXpalier_7,positionYpalier_7);
                //nouveau descente3
                positionXdescente_3+=-PAS_RAMPE;
                descente_3.setPosition(positionXdescente_3,positionYdescente_3);
                sleep( milliseconds(5));

                positionXdrapeau += -PAS_RAMPE;
                drapeau.setPosition(positionXdrapeau,positionYdrapeau);
            }
            }
        }
        //nouveau montee 3






        palier_1.setOrigin(LONGUEUR_PALIER_1,116);
        montee_2.setOrigin(214,210);
        montee_1.setOrigin(221.2,116.5);
        //nouveau palier 2
        palier_2.setOrigin(LONGUEUR_PALIER_2,210);
        //nouveau descente1
        descente_1.setOrigin(225,349);
        //nouveau montee3
        montee_3.setOrigin(493,342);
        //nouveau palier 4
        palier_4.setOrigin(100,342);
        //nouveau montee 4
        montee_4.setOrigin(578,162);
        //nouveau palier5
        palier_5.setOrigin(LONGUEUR_PALIER_5,positionYmontee_4);
        //nouveau descente 2

        //nouveaupalier6
        palier_6.setOrigin(LONGUEUR_PALIER_6,0);
        //nouveau montee 5
        //palier_7.setOrigin((LONGUEUR_PALIER_7,342))



        // disparition des �l�ments

        if (positionXmontee_1 == 0)
        {
            montee_1.setPosition(-8000,-2000);
        }

        if (positionXpalier_1 ==0)
        {
            palier_1.setPosition(-8000,-2000);
        }

        if (positionXmontee_2 == 0)
        {
            montee_2.setPosition(-8000,-2000);
        }
        //nouveau palier 2
        if(positionXpalier_2==0)
        {
            palier_2.setPosition(-8000,-2000);
        }
        //nouveau descente1
        if(positionXdescente_1==0)
        {
            descente_1.setPosition(-8000,-2000);
        }
        //nouveau palier 3
        if(positionXpalier_3==0)
        {
            palier_3.setPosition(-8000,-2000);
        }
        //nouveau montee3
        if(positionXmontee_3==0)
        {
            montee_3.setPosition(-8000,-2000);
        }
        //nouveau palier 4
        if(positionXpalier_4==0)
        {
            palier_4.setPosition(-8000,-2000);
        }
        //nouveau montee4
        if(positionXmontee_4==0)
        {
            montee_4.setPosition(-8000,-2000);
        }
        //nouveau palier5
        if(positionXpalier_5==0)
        {
            palier_5.setPosition(-8000,-2000);
        }
        //nouveau descente2
        if(positionXdescente_2==0)
        {
            descente_2.setPosition(-8000,-2000);
        }
        //nouveau paleir6
        if(positionXpalier_6==0)
        {
            palier_6.setPosition(-8000,-2000);
        }
        //nouveau palier 7
        if(positionXpalier_7==0)
        {
            palier_7.setPosition(-8000,-2000);
        }
        //nouveau descente3
        if (positionXdescente_3==0)
        {
            descente_3.setPosition(-8000,-2000);
        }
        //nouveau drapeau
        if(positionXdrapeau==0)
        {
            drapeau.setPosition(-8000,-2000);
        }


        if (jul.getGlobalBounds().intersects(montee_1.getGlobalBounds() ))
        {
            positionYJul-=1.2;
            jul.setPosition(positionXJul,positionYJul);

        }
        if (jul.getGlobalBounds().intersects(montee_2.getGlobalBounds() ))
        {
            positionYJul-=2;
            jul.setPosition(positionXJul,positionYJul);
        }
        if (positionXJul>=positionXdescente_1 && positionXJul<= positionXdescente_1+LONGUEUR_DESCENTE_1)
        {
            positionYJul+=4;
            jul.setPosition(positionXJul,positionYJul);
            if (jul.getGlobalBounds().intersects(rampe.getGlobalBounds()))
            {
                positionYJul=positionYRampe;
                jul.setPosition(positionXJul,positionYJul);
            }
        }
        if (jul.getGlobalBounds().intersects(montee_3.getGlobalBounds() ))
        {
            positionYJul-=1.38;
            jul.setPosition(positionXJul,positionYJul);
        }
        if (jul.getGlobalBounds().intersects(montee_4.getGlobalBounds() ))
        {
            positionYJul-=1;
            jul.setPosition(positionXJul,positionYJul);
        }
        if (positionXJul>=positionXdescente_2 && positionXJul<= positionXdescente_2+LONGUEUR_DESCENTE_2)
        {
            positionYJul+=3.6;
            jul.setPosition(positionXJul,positionYJul);
            if (jul.getGlobalBounds().intersects(rampe.getGlobalBounds()))
            {
                positionYJul=positionYRampe;
                jul.setPosition(positionXJul,positionYJul);
            }
        }
        if (jul.getGlobalBounds().intersects(montee_5.getGlobalBounds() ))
        {
            positionYJul-=2.5;
            jul.setPosition(positionXJul,positionYJul);
        }

        if (positionXJul>=positionXdescente_3 && positionXJul<= positionXdescente_3+LONGUEUR_DESCENTE_3)
        {
            positionYJul+=3.6;
            jul.setPosition(positionXJul,positionYJul);
            if (jul.getGlobalBounds().intersects(rampe.getGlobalBounds()))
            {
                positionYJul=positionYRampe;
                jul.setPosition(positionXJul,positionYJul);
            }
        }
        if (event.type == Event::MouseButtonReleased)
        {

            cabre =0 ;
        }

        if (event.type == Event::MouseButtonPressed)
        {
            cabre = 1 ;


        }

        app.clear();

        //rotation de jul

        if (cabre ==1 && rotation >=200)
        {
            if (rotation!=200)
            {
                jul.rotate(-PAS_T);
                rotation = jul.getRotation();
                printf("Rotation : %f if 3 \n",rotation);
            }
        }
        else
        {
            if (rotation<360 && rotation>270 )
            {
                jul.rotate(PAS_T);
                rotation = jul.getRotation();
                printf("Rotation : %f if 2 \n",rotation);
            }
            if(rotation<270 && rotation >= 200)
            {
                jul.rotate(-PAS_T);
                rotation = jul.getRotation();
                printf("rotation if 1 %f  \n ",rotation);
            }

        }

        if (rotation<200)
        {
            mort++;
        }
        if (positionXJul==positionXdrapeau)
        {
            gagne++;
        }


        if (choix=1)
        {

            // Draw the sprite
            app.draw(background);
            app.draw(rampe);
            app.draw(montee_1);
            app.draw(palier_1);
            app.draw(montee_2);
            //nouveau palier 2
            app.draw(palier_2);
            //nouveau descente1
            app.draw(descente_1);
            //nouveau montee3
            app.draw(palier_3);
            //nouveau montee3
            app.draw(montee_3);
            //nouveau palier4
            app.draw(palier_4);
            //nouveau montee4
            app.draw(montee_4);
            //nouveau palier5
            app.draw(palier_5);
            //nouveau descente2
            app.draw(descente_2);
            //nouveau palier6
            app.draw(palier_6);
            //nouveau montee 5
            app.draw(montee_5);
            //nouveau palier 7
            app.draw(palier_7);
            //nouveau descente3
            app.draw(descente_3);
            app.draw(drapeau);
            app.draw(jul);


        }

        if (mort>0)
            {
                app.draw(perdu);
            }
        if (gagne>0)
        {
            app.draw(victory);
            victory.setScale(1.5f,1.5f);
            perdu.setPosition(8000,8000);
        }


        app.display();
    }




    return EXIT_SUCCESS;
}
